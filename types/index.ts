export interface IVehiclePart {
  id: number;
  title: string;
  price: number;
}

export interface IData {
  body: IVehiclePart[];
  engine: IVehiclePart[];
  transmission: IVehiclePart[];
  accessories: IVehiclePart[];
}
