import { Typography } from '@mui/material';

export default function WizardWelcome() {
  return (
    <div>
      <Typography variant='h4'>Welcome!</Typography>
      <Typography variant='h5'>
        This builder will help you choose your new Renault Megane.
      </Typography>
    </div>
  );
}
