import { Card, CardContent, Typography } from '@mui/material';
import { Box } from '@mui/system';

import { useAppSelector } from '../../store';
import { IVehiclePart } from '../../types';
import TotalCost from '../total-cost';

export default function WizardConfirm() {
  const { body, engine, transmission, accessories } = useAppSelector(
    (state) => state.wizard
  );

  const cardComponent = (part: IVehiclePart) => {
    return (
      <Card sx={{ mb: 1, mt: 2 }}>
        <CardContent>
          <Typography variant='h6'>{part.title}</Typography>
          <Typography color='text.secondary'>£{part.price}</Typography>
        </CardContent>
      </Card>
    );
  };

  return (
    <div>
      <Typography
        variant='h4'
        sx={{ mb: 1 }}
      >{`Let's summarize your order:`}</Typography>
      <Box
        sx={{ borderTop: 1, borderBottom: 1, borderColor: 'grey.500' }}
        style={{ maxHeight: 300, overflow: 'auto', padding: 10 }}
      >
        <Typography variant='h6'>Body</Typography>
        {cardComponent(body)}
        <Typography variant='h6'>Engine</Typography>
        {cardComponent(engine)}
        <Typography variant='h6'>Transmission</Typography>
        {cardComponent(transmission)}
        <Typography variant='h6'>Accessories</Typography>
        {accessories.map((part) => cardComponent(part))}
      </Box>
      <TotalCost />
    </div>
  );
}
