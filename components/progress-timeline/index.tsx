import {
  Timeline,
  TimelineConnector,
  TimelineContent,
  TimelineDot,
  TimelineItem,
  TimelineSeparator,
} from '@mui/lab';
import { useAppSelector } from '../../store';

interface PropsType {
  title: string;
  id: number;
  slide: number;
  isLast?: boolean;
}

function TimelineItemComponent({ title, id, slide, isLast }: PropsType) {
  const isActive = slide === id;

  return (
    <TimelineItem>
      <TimelineSeparator>
        <TimelineDot color={isActive ? 'primary' : 'grey'} />
        {!isLast && <TimelineConnector />}
      </TimelineSeparator>
      <TimelineContent>{title}</TimelineContent>
    </TimelineItem>
  );
}

export default function ProgressTimeline() {
  const { slide } = useAppSelector((state) => state.wizard);
  return (
    <Timeline>
      <TimelineItemComponent id={0} slide={slide} title={'Welcome'} />
      <TimelineItemComponent id={1} slide={slide} title={'Main Options'} />
      <TimelineItemComponent id={2} slide={slide} title={'Accessories'} />
      <TimelineItemComponent id={3} slide={slide} title={'Confirm'} isLast />
    </Timeline>
  );
}
