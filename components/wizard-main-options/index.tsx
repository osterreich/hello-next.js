import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  Typography,
} from '@mui/material';

import { useAppDispatch, useAppSelector } from '../../store';
import { IWizardState, wizardActions } from '../../store/wizard';
import { IData, IVehiclePart } from '../../types';
import TotalCost from '../total-cost';

export default function WizardMainOptions() {
  const dispatch = useAppDispatch();

  const { wizard } = useAppSelector((state) => state);
  const { body, engine, transmission, data } = wizard;

  const onInputChange = (e: SelectChangeEvent<HTMLSelectElement>) => {
    const { name, value } = e.target;

    const partId = Number(value);
    const part = data[name as keyof IData].find((i) => i.id === partId);
    part && dispatch(wizardActions.setPart({ [name]: part }));
  };

  const formControlComponent = (key: string) => {
    const part = wizard[key as keyof IWizardState] as IVehiclePart;

    return (
      <FormControl key={key} variant='standard' sx={{ m: 1, minWidth: 300 }}>
        <InputLabel>{key}</InputLabel>
        <Select
          name={key}
          id={`${key}-select`}
          // @ts-ignore
          value={part.id}
          label='Age'
          onChange={onInputChange}
        >
          {data[key as keyof IData].map((item) => (
            <MenuItem key={item.id} value={item.id}>
              {item.title}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    );
  };

  return (
    <div>
      <Typography variant='h4'>Select main options of the car.</Typography>
      {Object.keys({ body, engine, transmission }).map((key) =>
        formControlComponent(key)
      )}
      <TotalCost />
    </div>
  );
}
