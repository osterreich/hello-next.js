import {
  Checkbox,
  FormControlLabel,
  FormGroup,
  Typography,
} from '@mui/material';
import { SyntheticEvent } from 'react';

import { useAppDispatch, useAppSelector } from '../../store';
import { wizardActions } from '../../store/wizard';
import { IVehiclePart } from '../../types';
import TotalCost from '../total-cost';

export default function WizardAccessories() {
  const dispatch = useAppDispatch();

  const { accessories, data } = useAppSelector((state) => state.wizard);

  const onInputChange = (e: SyntheticEvent) => {
    const { target } = e;

    if (!(target instanceof window.HTMLInputElement)) {
      return;
    }

    const accessoryId = Number(target.value);

    const part: IVehiclePart | undefined = data.accessories.find(
      (i) => i.id === accessoryId
    );

    if (part) {
      const accessoriesIds = accessories.map((i) => i.id);

      const updatedAccessories = accessoriesIds.includes(accessoryId)
        ? accessories.filter((i) => i.id !== accessoryId)
        : [...accessories, part];

      dispatch(wizardActions.setAccessories(updatedAccessories));
    }
  };

  return (
    <div>
      <Typography variant='h4'>Add accessories to your car.</Typography>
      <FormGroup sx={{ p: 1 }}>
        {data.accessories.map((item) => (
          <FormControlLabel
            key={item.id}
            control={<Checkbox defaultChecked />}
            label={item.title}
            value={item.id}
            checked={accessories.map((i) => i.id).includes(item.id)}
            onChange={onInputChange}
          />
        ))}
      </FormGroup>
      <TotalCost />
    </div>
  );
}
