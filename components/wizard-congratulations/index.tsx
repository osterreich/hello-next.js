import { Typography } from '@mui/material';

export default function WizardCongratulations() {
  return (
    <div>
      <Typography variant='h5'>Thank you!</Typography>
      <Typography>
        Your order is accepted, our manager will contact you shortly.
      </Typography>
    </div>
  );
}
