import { Chip, Typography } from '@mui/material';

import { useAppSelector } from '../../store';
import { IVehiclePart } from '../../types';

export default function TotalCost() {
  const { body, engine, transmission, accessories } = useAppSelector(
    (state) => state.wizard
  );

  const parts = [body, engine, transmission, ...accessories];
  const reducer = (previousValue: number, currentValue: IVehiclePart) =>
    previousValue + currentValue.price;

  const totalPrice = parts.reduce(reducer, 0);

  return (
    <Typography variant='h5' sx={{ mt: 3, borderColor: 'grey.500' }}>
      Total: £{totalPrice}
    </Typography>
  );
}
