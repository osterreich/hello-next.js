import { Button, Stack } from '@mui/material';

import { useAppDispatch, useAppSelector } from '../../store';
import { wizardActions } from '../../store/wizard';

interface PropsType {
  isCompleted: boolean;
}

export default function Navbar({ isCompleted }: PropsType) {
  const dispatch = useAppDispatch();
  const { slide } = useAppSelector((state) => state.wizard);

  return (
    <Stack direction={'row'} spacing={1} sx={{ justifyContent: 'center' }}>
      {!isCompleted ? (
        <>
          <Button
            variant='outlined'
            disabled={slide === 0}
            onClick={() => dispatch(wizardActions.setSlide(slide - 1))}
          >
            Back
          </Button>
          <Button
            variant='outlined'
            onClick={() => dispatch(wizardActions.setSlide(slide + 1))}
          >
            {slide === 3 ? 'Confirm order' : slide === 0 ? 'Start' : 'Next'}
          </Button>
        </>
      ) : (
        <Button
          variant='outlined'
          onClick={() => {
            dispatch(wizardActions.reset());
            dispatch(wizardActions.setSlide(0));
          }}
        >
          Start over
        </Button>
      )}
    </Stack>
  );
}
