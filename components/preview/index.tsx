export default function Preview() {
  return (
    <img
      alt='Renault'
      src='https://www.akpp-msk.com/assets/images/lab/ak1034.png'
      style={{ width: 200 }}
    />
  );
}
