import { Box, Container, Grid } from '@mui/material';
import type { NextPage } from 'next';
import Head from 'next/head';
import { useEffect, useState } from 'react';

import { useAppSelector } from '../store';
import ProgressTimeline from '../components/progress-timeline';
import WizardOptions from '../components/wizard-main-options';
import WizardAccessories from '../components/wizard-accessories';
import WizardWelcome from '../components/wizard-welcome';
import WizardConfirm from '../components/wizard-confirm';
import WizardCongratulations from '../components/wizard-congratulations';
import Navbar from '../components/navbar';
import Preview from '../components/preview';

const Wizard: NextPage = () => {
  const { slide, isCompleted } = useAppSelector((state) => state.wizard);

  const getStepComponent = () => {
    switch (slide) {
      case 0:
        return <WizardWelcome />;
      case 1:
        return <WizardOptions />;
      case 2:
        return <WizardAccessories />;
      case 3:
        return <WizardConfirm />;
      case 4:
        return <WizardCongratulations />;
      default:
        return null;
    }
  };

  return (
    <div>
      <Head>
        <title>Build your new Renault Megane</title>
        <meta name='description' content='Generated by create next app' />
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <main>
        <Container component='main'>
          <Grid container>
            <Grid item xs={12} sm={6}>
              <ProgressTimeline />
              <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
                <Navbar isCompleted={isCompleted} />
              </Box>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Preview />
              {getStepComponent()}
              <Box sx={{ display: { xs: 'block', sm: 'none' }, pt: 3, pb: 3 }}>
                <Navbar isCompleted={isCompleted} />
              </Box>
            </Grid>
          </Grid>
        </Container>
      </main>
    </div>
  );
};

export default Wizard;
