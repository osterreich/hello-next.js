import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { IData, IVehiclePart } from '../../types';

// Mock data
import data from '../../data.json';

export interface IWizardState {
  data: IData;
  body: IVehiclePart;
  engine: IVehiclePart;
  transmission: IVehiclePart;
  accessories: IVehiclePart[];
  slide: number;
  isCompleted: boolean;
}

const initialState: IWizardState = {
  data,
  body: data.body[0],
  engine: data.engine[0],
  transmission: data.transmission[0],
  accessories: [],
  slide: 0,
  isCompleted: false,
};

export const userSlice = createSlice({
  name: 'wizard',
  initialState,
  reducers: {
    setSlide: (state, action: PayloadAction<number>) => {
      return {
        ...state,
        slide: action.payload,
        isCompleted: action.payload === 4,
      };
    },
    setBody: (state, action: PayloadAction<IVehiclePart>) => {
      state.body = action.payload;
    },
    setEngine: (state, action: PayloadAction<IVehiclePart>) => {
      state.engine = action.payload;
    },
    setTransmission: (state, action: PayloadAction<IVehiclePart>) => {
      state.transmission = action.payload;
    },
    setAccessories: (state, action: PayloadAction<IVehiclePart[]>) => {
      state.accessories = action.payload;
    },
    setPart: (state, action: PayloadAction<Partial<IWizardState>>) => {
      return { ...state, ...action.payload };
    },
    reset: (state) => {
      return {
        ...state,
        body: data.body[0],
        engine: data.engine[0],
        transmission: data.transmission[0],
        accessories: [],
      };
    },
  },
});

export const { actions: wizardActions, reducer: wizardReducer } = userSlice;
