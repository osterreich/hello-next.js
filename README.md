Build your new Renault Megane
===

This is a [Next.js](https://nextjs.org/) test project.

To run project locally:
* Install node modules by running: `npm i`
* Launch dev server: `npm run dev`
* Follow [localhost:3000](http://localhost:3000) to open project in Browser

© 2022 Statut Systems | Nikita Filipenia
